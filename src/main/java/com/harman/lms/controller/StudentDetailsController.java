package com.harman.lms.controller;

import com.harman.lms.model.StudentDetails;
import com.harman.lms.service.StudentDetailsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/StudentDetailsService")
public class StudentDetailsController {
  @Autowired private StudentDetailsService studentDetailsService;

  /**
   * Find Student details by borrower Id.
   *
   * @param borrowerId as PathVariable
   * @return
   */
  @RequestMapping(value = "/studentDetails/{borrowerId}", method = RequestMethod.GET)
  public ResponseEntity<StudentDetails> findStudentByBorrowerId(
      @PathVariable("borrowerId") final Long borrowerId) {
    ResponseEntity<StudentDetails> response = null;
    final StudentDetails studentDetails = studentDetailsService.findStudentByBorrowerId(borrowerId);
    if (studentDetails != null) {
      response = new ResponseEntity<StudentDetails>(studentDetails, HttpStatus.OK);
    } else {
      response = new ResponseEntity<StudentDetails>(studentDetails, HttpStatus.NO_CONTENT);
    }
    return response;
  }

  /**
   * List all the Students.
   *
   * @return
   */
  @RequestMapping(value = "/studentDetails", method = RequestMethod.GET)
  public ResponseEntity<List<StudentDetails>> findAllStudentDetails() {
    return new ResponseEntity<List<StudentDetails>>(
        studentDetailsService.findAllStudents(), HttpStatus.OK);
  }

  /**
   * Add Student Details.
   *
   * @param studentDetails as input in jason format
   * @return
   */
  @RequestMapping(value = "/studentDetails", method = RequestMethod.POST)
  public ResponseEntity<StudentDetails> addStudentDetails(
      @RequestBody final StudentDetails studentDetails) {
    ResponseEntity<StudentDetails> response = null;
    final StudentDetails studentDtls = studentDetailsService.addStudentDetails(studentDetails);
    response = new ResponseEntity<StudentDetails>(studentDtls, HttpStatus.OK);
    return response;
  }
}
