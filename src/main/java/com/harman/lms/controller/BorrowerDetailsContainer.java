package com.harman.lms.controller;

import com.harman.lms.model.BorrowerDetails;
import com.harman.lms.service.BorrowerDetailsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/BorrowerDetailsService")
public class BorrowerDetailsContainer {
  @Autowired private BorrowerDetailsService borrowerDetailsService;

  /**
   * Borrow Book Details.
   *
   * @param borrowerDetails as input in jason format
   * @return
   */
  @RequestMapping(value = "/borrowBook", method = RequestMethod.POST)
  public String borrowBook(@RequestBody final BorrowerDetails borrowerDetails) {
    return borrowerDetailsService.borrowBook(borrowerDetails);
  }

  /**
   * Return Book. Update the actual return date.
   *
   * @param bookId is isbnCode of the book
   * @param borrowerId is the borrowerId of the student
   * @return
   */
  @RequestMapping(value = "/returnBook/{bookId}/{borrowerId}", method = RequestMethod.PUT)
  public void returnBook(
      @PathVariable("bookId") final Long bookId,
      @PathVariable("borrowerId") final Long borrowerId) {
    borrowerDetailsService.returnBook(bookId, borrowerId);
  }

  /**
   * get all the borrower details.
   *
   * @return
   */
  @RequestMapping(value = "/borrowerDetails", method = RequestMethod.GET)
  public List<BorrowerDetails> getAllBorrowerDetails() {
    return borrowerDetailsService.getAllBorrowerDetails();
  }

  /**
   * get all the borrower details.
   *
   * @return
   */
  @RequestMapping(value = "/borrowerDetails/reachedDueDate", method = RequestMethod.GET)
  public List<BorrowerDetails> getBorrowersReachedDueDate() {
    return borrowerDetailsService.getBorrowersReachedDueDate();
  }
}
