package com.harman.lms.controller;

import com.harman.lms.model.BindingDetails;
import com.harman.lms.service.BindingDetailsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/BindingDetailsService")
public class BindingDetailsController {
  @Autowired private BindingDetailsService bindingDetailsService;

  /**
   * Find binding details by binding Id.
   *
   * @param bindingId as PathVariable
   * @return
   */
  @RequestMapping(value = "/bindingDetails/{bindingId}", method = RequestMethod.GET)
  public ResponseEntity<BindingDetails> findBindingById(
      @PathVariable("bindingId") final Long bindingId) {
    ResponseEntity<BindingDetails> response = null;
    final BindingDetails bindingDetails = bindingDetailsService.findBindingById(bindingId);
    if (bindingDetails != null) {
      response = new ResponseEntity<BindingDetails>(bindingDetails, HttpStatus.OK);
    } else {
      response = new ResponseEntity<BindingDetails>(bindingDetails, HttpStatus.NO_CONTENT);
    }
    return response;
  }

  /**
   * List all the binding details.
   *
   * @return
   */
  @RequestMapping(value = "/bindingDetails", method = RequestMethod.GET)
  public ResponseEntity<List<BindingDetails>> findAllBindings() {
    return new ResponseEntity<List<BindingDetails>>(
        bindingDetailsService.findAllBindings(), HttpStatus.OK);
  }

  /**
   * Add binding Details.
   *
   * @param bindingDetails as input in jason format
   * @return
   */
  @RequestMapping(value = "/bindingDetails", method = RequestMethod.POST)
  public ResponseEntity<BindingDetails> addBindingDetails(
      @RequestBody final BindingDetails bindingDetails) {
    ResponseEntity<BindingDetails> response = null;
    final BindingDetails bindingDtls = bindingDetailsService.addBindingDetails(bindingDetails);
    response = new ResponseEntity<BindingDetails>(bindingDtls, HttpStatus.OK);
    return response;
  }

  /**
   * Delete binding by bindingId.
   *
   * @param bindingId as input
   * @return
   */
  @RequestMapping(value = "/bindingDetails/{bindingId}", method = RequestMethod.DELETE)
  public void deleteBindingById(@PathVariable("bindingId") final Long bindingId) {
    bindingDetailsService.deleteBindingById(bindingId);
  }

  /**
   * Delete All bindings.
   *
   * @return
   */
  @RequestMapping(value = "/bindingDetails", method = RequestMethod.DELETE)
  public void deleteAllBindings() {
    bindingDetailsService.deleteAllBindings();
  }

  /**
   * Update binding.
   *
   * @param bindingDetails as input in json format
   * @return
   */
  @RequestMapping(value = "/bindingDetails", method = RequestMethod.PUT)
  public ResponseEntity<BindingDetails> updateBindingDetails(
      @RequestBody final BindingDetails bindingDetails) {
    ResponseEntity<BindingDetails> response = null;
    final BindingDetails bindingDtls = bindingDetailsService.updateBinding(bindingDetails);
    response = new ResponseEntity<BindingDetails>(bindingDtls, HttpStatus.OK);
    return response;
  }
}
