package com.harman.lms.controller;

import com.harman.lms.model.ShelfDetails;
import com.harman.lms.service.ShelfDetailsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ShelfDetailsService")
public class ShelfDetailsController {
  @Autowired private ShelfDetailsService shelfDetailsService;

  /**
   * Add Shelf Details.
   *
   * @param shelfDetails as input in jason format
   * @return
   */
  @RequestMapping(value = "/shelfDetails", method = RequestMethod.POST)
  public ResponseEntity<ShelfDetails> addShelfDetails(
      @RequestBody final ShelfDetails shelfDetails) {
    return new ResponseEntity<ShelfDetails>(
        shelfDetailsService.addShelfDetails(shelfDetails), HttpStatus.OK);
  }

  /**
   * Find shelf details by staff Id.
   *
   * @param shelfId as PathVariable
   * @return
   */
  @RequestMapping(value = "/shelfDetails/{shelfId}", method = RequestMethod.GET)
  public ResponseEntity<ShelfDetails> findShelfById(@PathVariable("shelfId") final Long shelfId) {
    ResponseEntity<ShelfDetails> response = null;
    final ShelfDetails shelfDetails = shelfDetailsService.findShelfDetailsById(shelfId);
    if (shelfDetails != null) {
      response = new ResponseEntity<ShelfDetails>(shelfDetails, HttpStatus.OK);
    } else {
      response = new ResponseEntity<ShelfDetails>(shelfDetails, HttpStatus.NO_CONTENT);
    }
    return response;
  }

  /**
   * List all the shelf details.
   *
   * @return
   */
  @RequestMapping(value = "/shelfDetails", method = RequestMethod.GET)
  public ResponseEntity<List<ShelfDetails>> findAllShelfDetails() {
    return new ResponseEntity<List<ShelfDetails>>(
        shelfDetailsService.findAllShelfDetails(), HttpStatus.OK);
  }
}
