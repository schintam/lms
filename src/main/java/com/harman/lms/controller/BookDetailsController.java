package com.harman.lms.controller;

import com.harman.lms.model.BookDetails;
import com.harman.lms.service.BookDetailsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/BookDetailsService")
public class BookDetailsController {
  @Autowired private BookDetailsService bookDetailsService;

  /**
   * Find book details by book Id.
   *
   * @param bookId as PathVariable
   * @return
   */
  @RequestMapping(value = "/bookDetails/{bookId}", method = RequestMethod.GET)
  public ResponseEntity<BookDetails> findBookById(@PathVariable("bookId") final Long bookId) {
    ResponseEntity<BookDetails> response = null;
    final BookDetails bookDetails = bookDetailsService.findBookById(bookId);
    if (bookDetails != null) {
      response = new ResponseEntity<BookDetails>(bookDetails, HttpStatus.OK);
    } else {
      response = new ResponseEntity<BookDetails>(bookDetails, HttpStatus.NO_CONTENT);
    }
    return response;
  }

  /**
   * List all the book details.
   *
   * @return
   */
  @RequestMapping(value = "/bookDetails", method = RequestMethod.GET)
  public ResponseEntity<List<BookDetails>> findAllBooks() {
    return new ResponseEntity<List<BookDetails>>(bookDetailsService.findAllBooks(), HttpStatus.OK);
  }

  /**
   * Add book Details.
   *
   * @param bookDetails as input in jason format
   * @return
   */
  @RequestMapping(value = "/bookDetails", method = RequestMethod.POST)
  public ResponseEntity<BookDetails> addBookDetails(@RequestBody final BookDetails bookDetails) {
    ResponseEntity<BookDetails> response = null;
    final BookDetails bookDtls = bookDetailsService.addBookDetails(bookDetails);
    response = new ResponseEntity<BookDetails>(bookDtls, HttpStatus.OK);
    return response;
  }

  /**
   * Delete book by bookId.
   *
   * @param bookId as input
   * @return
   */
  @RequestMapping(value = "/bookDetails/{bookId}", method = RequestMethod.DELETE)
  public void deleteBookById(@PathVariable("bookId") final Long bookId) {
    bookDetailsService.deleteBookById(bookId);
  }

  /**
   * Delete All books.
   *
   * @return
   */
  @RequestMapping(value = "/bookDetails", method = RequestMethod.DELETE)
  public void deleteAllBooks() {
    bookDetailsService.deleteAllBooks();
  }

  /**
   * Update book.
   *
   * @param bookDetails as input in json format
   * @return
   */
  @RequestMapping(value = "/bookDetails", method = RequestMethod.PUT)
  public ResponseEntity<BookDetails> updateBookDetails(@RequestBody final BookDetails bookDetails) {
    ResponseEntity<BookDetails> response = null;
    final BookDetails bookDtls = bookDetailsService.updateBook(bookDetails);
    response = new ResponseEntity<BookDetails>(bookDtls, HttpStatus.OK);
    return response;
  }
}
