package com.harman.lms.controller;

import com.harman.lms.model.CategoryDetails;
import com.harman.lms.service.CategoryDetailsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/CategoryDetailsService")
public class CategoryDetailsController {
  @Autowired private CategoryDetailsService categoryDetailsService;

  /**
   * Find category details by category Id.
   *
   * @param categoryId as PathVariable
   * @return
   */
  @RequestMapping(value = "/categoryDetails/{categoryId}", method = RequestMethod.GET)
  public ResponseEntity<CategoryDetails> findCategoryById(
      @PathVariable("categoryId") final Long categoryId) {
    ResponseEntity<CategoryDetails> response = null;
    final CategoryDetails categoryDetails = categoryDetailsService.findCategoryById(categoryId);
    if (categoryDetails != null) {
      response = new ResponseEntity<CategoryDetails>(categoryDetails, HttpStatus.OK);
    } else {
      response = new ResponseEntity<CategoryDetails>(categoryDetails, HttpStatus.NO_CONTENT);
    }
    return response;
  }

  /**
   * List all the category details.
   *
   * @return
   */
  @RequestMapping(value = "/categoryDetails", method = RequestMethod.GET)
  public ResponseEntity<List<CategoryDetails>> findAllCategories() {
    return new ResponseEntity<List<CategoryDetails>>(
        categoryDetailsService.findAllCategories(), HttpStatus.OK);
  }

  /**
   * Add category Details.
   *
   * @param categoryDetails as input in jason format
   * @return
   */
  @RequestMapping(value = "/categoryDetails", method = RequestMethod.POST)
  public ResponseEntity<CategoryDetails> addCategoryDetails(
      @RequestBody final CategoryDetails categoryDetails) {
    ResponseEntity<CategoryDetails> response = null;
    final CategoryDetails categoryDtls = categoryDetailsService.addCategoryDetails(categoryDetails);
    response = new ResponseEntity<CategoryDetails>(categoryDtls, HttpStatus.OK);
    return response;
  }

  /**
   * Delete category by categoryId.
   *
   * @param categoryId as input
   * @return
   */
  @RequestMapping(value = "/categoryDetails/{categoryId}", method = RequestMethod.DELETE)
  public void deleteCategoryById(@PathVariable("categoryId") final Long categoryId) {
    categoryDetailsService.deleteCategoryById(categoryId);
  }

  /**
   * Delete All categories.
   *
   * @return
   */
  @RequestMapping(value = "/categoryDetails", method = RequestMethod.DELETE)
  public void deleteAllCategories() {
    categoryDetailsService.deleteAllCategories();
  }

  /**
   * Update category.
   *
   * @param categoryDetails as input in json format
   * @return
   */
  @RequestMapping(value = "/categoryDetails", method = RequestMethod.PUT)
  public ResponseEntity<CategoryDetails> updateCategoryDetails(
      @RequestBody final CategoryDetails categoryDetails) {
    ResponseEntity<CategoryDetails> response = null;
    final CategoryDetails categoryDtls = categoryDetailsService.updateCategory(categoryDetails);
    response = new ResponseEntity<CategoryDetails>(categoryDtls, HttpStatus.OK);
    return response;
  }
}
