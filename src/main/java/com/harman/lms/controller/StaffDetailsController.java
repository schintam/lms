package com.harman.lms.controller;

import com.harman.lms.model.StaffDetails;
import com.harman.lms.service.StaffDetailsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/StaffDetailsService")
public class StaffDetailsController {
  @Autowired private StaffDetailsService staffDetailsService;

  /**
   * Find staff details by staff Id.
   *
   * @param staffId as PathVariable
   * @return
   */
  @RequestMapping(value = "/staffDetails/{staffId}", method = RequestMethod.GET)
  public ResponseEntity<StaffDetails> findStaffById(@PathVariable("staffId") final Long staffId) {
    System.out.println("staffId: " + staffId);
    ResponseEntity<StaffDetails> response = null;
    final StaffDetails staffDetails = staffDetailsService.findStaffById(staffId);
    if (staffDetails != null) {
      response = new ResponseEntity<StaffDetails>(staffDetails, HttpStatus.OK);
    } else {
      response = new ResponseEntity<StaffDetails>(staffDetails, HttpStatus.NO_CONTENT);
    }
    return response;
  }

  /**
   * List all the staff details.
   *
   * @return
   */
  @RequestMapping(value = "/staffDetails", method = RequestMethod.GET)
  public ResponseEntity<List<StaffDetails>> findAllStaffDetails() {
    return new ResponseEntity<List<StaffDetails>>(
        staffDetailsService.findAllStaffDetails(), HttpStatus.OK);
  }

  /**
   * Add staff Details.
   *
   * @param staffDetails as input in jason format
   * @return
   */
  @RequestMapping(value = "/staffDetails", method = RequestMethod.POST)
  public ResponseEntity<StaffDetails> addStaffDetails(
      @RequestBody final StaffDetails staffDetails) {
    ResponseEntity<StaffDetails> response = null;
    final StaffDetails staffDtls = staffDetailsService.addStaffDetails(staffDetails);
    response = new ResponseEntity<StaffDetails>(staffDtls, HttpStatus.OK);
    return response;
  }
}
