package com.harman.lms.service;

import com.harman.lms.dao.CategoryDetailsDao;
import com.harman.lms.model.CategoryDetails;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryDetailsService {
  @Autowired private CategoryDetailsDao categoryDetailsDao;

  public CategoryDetails addCategoryDetails(final CategoryDetails categoryDetails) {
    return categoryDetailsDao.save(categoryDetails);
  }

  /**
   * get category details by category id.
   *
   * @param categoryId as input
   * @return
   */
  public CategoryDetails findCategoryById(final Long categoryId) {
    final Optional<CategoryDetails> categoryDetailsOptional =
        categoryDetailsDao.findById(categoryId);
    return categoryDetailsOptional.orElse(null);
  }

  public List<CategoryDetails> findAllCategories() {
    return categoryDetailsDao.findAll();
  }

  public void deleteCategoryById(final Long categoryId) {
    categoryDetailsDao.deleteById(categoryId);
  }

  public void deleteAllCategories() {
    categoryDetailsDao.deleteAll();
  }

  public CategoryDetails updateCategory(final CategoryDetails categoryDetails) {
    return categoryDetailsDao.save(categoryDetails);
  }
}
