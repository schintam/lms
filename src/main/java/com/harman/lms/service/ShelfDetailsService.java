package com.harman.lms.service;

import com.harman.lms.dao.ShelfDetailsDao;
import com.harman.lms.model.ShelfDetails;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShelfDetailsService {
  @Autowired private ShelfDetailsDao shelfDetailsDao;

  public ShelfDetails addShelfDetails(final ShelfDetails shelfDetails) {
    return shelfDetailsDao.save(shelfDetails);
  }

  public ShelfDetails findShelfDetailsById(final Long shelfId) {
    final Optional<ShelfDetails> shelfDetailsOptional = shelfDetailsDao.findById(shelfId);
    return shelfDetailsOptional.orElse(null);
  }

  public List<ShelfDetails> findAllShelfDetails() {
    return shelfDetailsDao.findAll();
  }
}
