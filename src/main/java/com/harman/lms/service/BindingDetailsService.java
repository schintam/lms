package com.harman.lms.service;

import com.harman.lms.dao.BindingDetailsDao;
import com.harman.lms.model.BindingDetails;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BindingDetailsService {
  @Autowired private BindingDetailsDao bindingDetailsDao;

  public BindingDetails addBindingDetails(final BindingDetails bindingDetails) {
    return bindingDetailsDao.save(bindingDetails);
  }

  public BindingDetails findBindingById(final Long bindingId) {
    final Optional<BindingDetails> bindingDetailsOptional = bindingDetailsDao.findById(bindingId);
    return bindingDetailsOptional.orElse(null);
  }

  public List<BindingDetails> findAllBindings() {
    return bindingDetailsDao.findAll();
  }

  public void deleteBindingById(final Long bindingId) {
    bindingDetailsDao.deleteById(bindingId);
  }

  public void deleteAllBindings() {
    bindingDetailsDao.deleteAll();
  }

  public BindingDetails updateBinding(final BindingDetails bindingDetails) {
    return bindingDetailsDao.save(bindingDetails);
  }
}
