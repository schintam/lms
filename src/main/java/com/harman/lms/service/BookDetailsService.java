package com.harman.lms.service;

import com.harman.lms.dao.BookDetailsDao;
import com.harman.lms.model.BookDetails;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookDetailsService {
  @Autowired private BookDetailsDao bookDetailsDao;

  public BookDetails addBookDetails(final BookDetails bookDetails) {
    return bookDetailsDao.save(bookDetails);
  }

  public BookDetails findBookById(final Long isbnCode) {
    final Optional<BookDetails> bookDetailsOptional = bookDetailsDao.findById(isbnCode);
    return bookDetailsOptional.orElse(null);
  }

  public List<BookDetails> findAllBooks() {
    return bookDetailsDao.findAll();
  }

  public void deleteBookById(final Long bindingId) {
    bookDetailsDao.deleteById(bindingId);
  }

  public void deleteAllBooks() {
    bookDetailsDao.deleteAll();
  }

  public BookDetails updateBook(final BookDetails bookDetails) {
    return bookDetailsDao.save(bookDetails);
  }
}
