package com.harman.lms.service;

import com.harman.lms.dao.StudentDetailsDao;
import com.harman.lms.model.StudentDetails;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDetailsService {
  @Autowired private StudentDetailsDao studentDetailsDao;

  public StudentDetails addStudentDetails(final StudentDetails studentDetails) {
    return studentDetailsDao.save(studentDetails);
  }

  public StudentDetails findStudentByBorrowerId(final Long borrowerId) {
    final Optional<StudentDetails> studentDetails = studentDetailsDao.findById(borrowerId);
    return studentDetails.orElse(null);
  }

  public List<StudentDetails> findAllStudents() {
    return studentDetailsDao.findAll();
  }
}
