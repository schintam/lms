package com.harman.lms.service;

import com.harman.lms.dao.BookDetailsDao;
import com.harman.lms.dao.BorrowerDetailsDao;
import com.harman.lms.model.BookDetails;
import com.harman.lms.model.BorrowerDetails;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BorrowerDetailsService {
  @Autowired private BorrowerDetailsDao borrowerDetailsDao;
  @Autowired private BookDetailsDao bookDetailsDao;

  /**
   * Add Borrower Details / Issue Book to the student.
   *
   * @param borrowerDetails contains Book issue details
   * @return
   */
  public String borrowBook(final BorrowerDetails borrowerDetails) {
    final BorrowerDetails borrowerDtls =
        borrowerDetailsDao.getBorrowerDetails(
            borrowerDetails.getBorrowerId(), borrowerDetails.getBookId());
    if (borrowerDtls != null) {
      return "Book already Borrowed";
    }
    final Optional<BookDetails> bookDetailsOptional =
        bookDetailsDao.findById(borrowerDetails.getBookId());
    final BookDetails bookDetails = bookDetailsOptional.orElse(null);
    if (bookDetails != null && bookDetails.getNoOfCopiesCurrent() > 0) {
      System.out.println("Book available");
      borrowerDetailsDao.save(borrowerDetails);
      bookDetails.setNoOfCopiesCurrent(bookDetails.getNoOfCopiesCurrent() - 1);
      bookDetailsDao.save(bookDetails);
      return "Success";
    } else {
      System.out.println("Book not available");
      return "Book Not available";
    }
  }

  /**
   * Return Book.
   *
   * @param bookId is isbnCode
   * @param borrowerId is id of the borrower
   */
  public void returnBook(final Long bookId, final Long borrowerId) {
    final BorrowerDetails borrowerDtls = borrowerDetailsDao.getBorrowerDetails(borrowerId, bookId);
    borrowerDtls.setActulaReturnDate(LocalDate.now());
    borrowerDetailsDao.save(borrowerDtls);

    final Optional<BookDetails> bookDetailsOptional = bookDetailsDao.findById(bookId);
    final BookDetails bookDetails = bookDetailsOptional.orElse(null);
    bookDetails.setNoOfCopiesCurrent(bookDetails.getNoOfCopiesCurrent() + 1);
    bookDetailsDao.save(bookDetails);
  }

  public List<BorrowerDetails> getAllBorrowerDetails() {
    return borrowerDetailsDao.findAll();
  }

  public List<BorrowerDetails> getBorrowersReachedDueDate() {
    return borrowerDetailsDao.getBorrowersReachedDueDate();
  }
}
