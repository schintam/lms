package com.harman.lms.service;

import com.harman.lms.dao.StaffDetailsDao;
import com.harman.lms.model.StaffDetails;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StaffDetailsService {
  @Autowired private StaffDetailsDao staffDetailsDao;

  public StaffDetails addStaffDetails(final StaffDetails staffDetails) {
    return staffDetailsDao.save(staffDetails);
  }

  public StaffDetails findStaffById(final Long staffId) {
    final Optional<StaffDetails> staffDetails = staffDetailsDao.findById(staffId);
    return staffDetails.orElse(null);
  }

  public List<StaffDetails> findAllStaffDetails() {
    return staffDetailsDao.findAll();
  }
}
