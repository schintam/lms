package com.harman.lms.dao;

import com.harman.lms.model.StaffDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffDetailsDao extends JpaRepository<StaffDetails, Long> {}
