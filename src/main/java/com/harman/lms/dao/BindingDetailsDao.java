package com.harman.lms.dao;

import com.harman.lms.model.BindingDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BindingDetailsDao extends JpaRepository<BindingDetails, Long> {}
