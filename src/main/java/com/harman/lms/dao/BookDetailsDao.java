package com.harman.lms.dao;

import com.harman.lms.model.BookDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookDetailsDao extends JpaRepository<BookDetails, Long> {}
