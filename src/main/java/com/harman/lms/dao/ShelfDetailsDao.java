package com.harman.lms.dao;

import com.harman.lms.model.ShelfDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShelfDetailsDao extends JpaRepository<ShelfDetails, Long> {}
