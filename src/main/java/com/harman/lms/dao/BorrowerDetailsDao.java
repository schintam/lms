package com.harman.lms.dao;

import com.harman.lms.model.BorrowerDetails;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BorrowerDetailsDao extends JpaRepository<BorrowerDetails, Long> {
  @Query(
      value =
          "select * from borrower_details b where b.Borrower_Id =:borrowerId and "
              + "Book_Id=:bookId and Actual_Return_Date is null",
      nativeQuery = true)
  BorrowerDetails getBorrowerDetails(
      @Param("borrowerId") Long borrowerId, @Param("bookId") Long bookId);

  @Query(
      value =
          "select * from borrower_details where "
              + "Actual_Return_Date is null and Borrowed_TO<CURDATE()",
      nativeQuery = true)
  List<BorrowerDetails> getBorrowersReachedDueDate();
}
