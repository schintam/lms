package com.harman.lms.dao;

import com.harman.lms.model.CategoryDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDetailsDao extends JpaRepository<CategoryDetails, Long> {}
