package com.harman.lms.dao;

import com.harman.lms.model.StudentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDetailsDao extends JpaRepository<StudentDetails, Long> {}
