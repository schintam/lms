package com.harman.lms.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "shelf_details")
public class ShelfDetails implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Shelf_id")
  private Long shelfId;

  @Column(name = "Shelf_No")
  private int shelfNumber;

  @Column(name = "Floor_No")
  private int floorNumber;
}
