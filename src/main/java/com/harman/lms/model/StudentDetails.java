package com.harman.lms.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "student_details")
public class StudentDetails implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "Student_Id")
  private String studentId;

  @Column(name = "Student_Name")
  private String studentName;

  @Column(name = "Sex")
  private String sex;

  @Column(name = "Date_Of_Birth")
  private LocalDate dateOfBirth;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Borrower_Id")
  private Long borrowerId;

  @Column(name = "Department")
  private String department;

  @Column(name = "contact_Number")
  private String contactNumber;
}
