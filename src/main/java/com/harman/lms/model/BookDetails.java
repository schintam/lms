package com.harman.lms.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "book_details")
public class BookDetails implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ISBN_Code")
  private Long isbnCode;

  @Column(name = "Book_Title")
  private String bookTitle;

  @Column(name = "Language")
  private String language;

  @Column(name = "Binding_Id")
  private int bindingId;

  @Column(name = "No_Copies_Actual")
  private int noOfCopiesActula;

  @Column(name = "No_Copies_Current")
  private int noOfCopiesCurrent;

  @Column(name = "Category_id")
  private int categoryId;

  @Column(name = "Publication_year")
  private int publicationYear;

  @Column(name = "Shelf_Id")
  private int shelfId;
}
