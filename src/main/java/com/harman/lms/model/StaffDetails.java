package com.harman.lms.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "staff_details")
public class StaffDetails implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Staff_Id")
  private Long staffId;

  @Column(name = "Staff_Name")
  private String staffName;

  @Column(name = "Password")
  private String password;

  @Column(name = "Is_Admin")
  private boolean admin;

  @Column(name = "Designation")
  private String designation;
}
