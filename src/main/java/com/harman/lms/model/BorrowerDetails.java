package com.harman.lms.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "borrower_details")
public class BorrowerDetails implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "id")
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "Borrower_Id")
  private Long borrowerId;

  @Column(name = "Book_Id")
  private Long bookId;

  @Column(name = "Borrowed_From")
  private LocalDate borrowedFrom;

  @Column(name = "Borrowed_TO")
  private LocalDate borrowedTo;

  @Column(name = "Actual_Return_Date")
  private LocalDate actulaReturnDate;

  @Column(name = "Issued_by")
  private int issuedBy;
}
